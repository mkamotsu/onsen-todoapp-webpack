require('angular');
require('onsenui/css/onsenui');
require('onsenui/css/onsen-css-components');
require('onsenui/js/onsenui');
require('./index.less');

declare interface Window {
  nav: NavigatorView;
  tab: TabbarView;
}

interface Todo {
  text: string,
  done: boolean
}

class TodoListController {
  private todos: Todo[];
  private todoText: string;

  public addTodo() {
    this.todos.push({text: this.todoText, done: false});
    this.todoText = '';
  };

  public remaining() {
    let count = 0;
    angular.forEach(this.todos, function(todo) {
      count += todo.done ? 0 : 1;
    });
    return count;
  };

  public archive() {
    let oldTodos = this.todos;
    this.todos = [];
    angular.forEach(oldTodos, todo => {
      if (!todo.done) this.todos.push(todo);
    });
  };

  constructor() {
    this.todos = [
      { text: 'learn AngularJS', done:true },
      { text: 'build an AngularJS app', done:false }
    ];
  }
}

ons.bootstrap('todo', ['onsen'])
.controller(
  'TodoListController',
  [
    '$window',
    () => new TodoListController()
  ]
);
