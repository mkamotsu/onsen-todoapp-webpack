import * as webpack from 'webpack';

const config: webpack.Configuration = {
  entry: {
    main: './index.ts'
  },
  output: {
    filename: 'index.js'
  },
  module: {
    loaders: [
      { test: /\.tsx?$/, loader: 'ts-loader' },
      { test: /onsenui\.js$/, loader: 'imports-loader?this=>window,define=>false,module=>undefined' },
      { test: /\.css$/, loaders: ['style-loader', 'css-loader?sourceMap'] },
      {
        test: /\.less$/,
        loaders: [
          'style-loader', 'css-loader?sourceMap=true', 'less-loader?sourceMap=true'
        ]
      },
      {
        test: /\.(ttf|eot|svg|woff)$/,
        loader: 'url-loader',
        options: { limit: 8192 }
      }
    ]
  },
  resolve: {
    extensions: ['.js', '.ts', '.tsx', '.css'],
    modules: [
      'node_modules/onsenui/css',
      'node_modules'
    ]
  }
}

export default config;
